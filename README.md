https://www.valentinog.com/blog/webpack/#getting-started-with-webpack

https://www.toptal.com/react/webpack-react-tutorial-pt-1
https://www.toptal.com/react/webpack-config-tutorial-pt-2


npm install --save-dev -D webpack webpack-cli webpack-dev-server

npm install @babel/core babel-loader @babel/preset-env @babel/preset-react --save-dev
npm install @babel/plugin-transform-runtime @babel/plugin-proposal-class-properties --save-dev

npm install --save-dev clean-webpack-plugin copy-webpack-plugin
npm install css-loader style-loader sass-loader sass --save-dev

npm install html-webpack-plugin terser-webpack-plugin --save-dev
npm install react react-dom --save-dev
npm install file-loader --save-dev
