module.exports = require('../webpack.config-helper')({
    isProduction: false,
    ignoreAssetsBuild: true,
    devtool: 'cheap-module-eval-source-map',
    // port: 3008
});
