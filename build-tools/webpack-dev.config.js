module.exports = require('../webpack.config-helper')({
    isProduction: false,
    ignoreAssetsBuild: false,
    devtool: 'cheap-module-eval-source-map',
    // port: 3008
});
