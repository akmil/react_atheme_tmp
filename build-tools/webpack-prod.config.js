module.exports = require('../webpack.config-helper')({
    ignoreAssetsBuild: false,
    isProduction: true,
    devtool: false
});
