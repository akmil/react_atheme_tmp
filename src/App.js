import React from 'react';
import { ProvideMainContext } from './main/contexts/MainContext';
import Routes from './main/router/Routes';

function App() {
  return (
    <ProvideMainContext>
      <Routes />
    </ProvideMainContext>
  );
}

export default App;
