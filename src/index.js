import React from 'react';
import ReactDOM from 'react-dom';
/* const testUrl = "./App";
import App from testUrl; */
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
