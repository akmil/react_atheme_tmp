const url = Object.freeze({
  LOGIN_URL: 'http://localhost:4008/api/v1/auth/login/',
  DELETE_TOKEN: 'http://localhost:4008/api/v1/auth/del_token/',
  REFRESH_TOKEN: 'http://localhost:4008/api/v1/auth/access_token/',
});

const app = {
  title: 'ReactATheme',
  logo: 'logo.webp',
  favicon: 'favicon.ico',
};
export default { app, url };
