import React from 'react';
import Dashboard from '../../module/Pages/dashboard/Dashboard';
import NotFound from '../../module/Pages/NotFound';
import Settings from '../../module/Pages/settings/Settings';
import Login from '../../module/Pages/login/Login';
/* todo: load from main or copy, to make it independent */

const routesToPages = (key) => {
  const mapOfRoutes = {
    dashboard: <Dashboard />,
    settings: <Settings />,
    login: <Login />,
  };

  return (mapOfRoutes[key]) ? mapOfRoutes[key] : <NotFound />;
};

export default routesToPages;
