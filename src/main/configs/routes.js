/* import React, { Suspense } from 'react'; */

/* const Login = React.lazy(() => import(/!* webpackChunkName: "Login" *!/'../../module/Pages/login/Login')); */
/* const Dashboard = React.lazy(() => import(/!* webpackChunkName: "Dashboard" *!/'../../module/Pages/dashboard/Dashboard')); */
/* const Settings = React.lazy(() => import(/!* webpackChunkName: "Settings" *!/'../../module/Pages/settings/Settings')); */

const ROUTES = {
  dashboard: {
    path: '/',
    exact: true,
    isPublic: false,
    faIcon: 'fas fa-tachometer-alt',
    menuItemValue: 'Dashboard',
    pageIdentifier: 'dashboard',
    // sidebar: () => <div>Dashboard</div>,
    /* main: () => (
      <Suspense fallback={<div>Loading page...</div>}>
        <Dashboard />
      </Suspense>
    ), */
  },
  settings: {
    path: '/settings',
    exact: true,
    isPublic: false,
    faIcon: 'fas fa-map-marked-alt',
    menuItemValue: 'Settings',
    pageIdentifier: 'settings',
    // sidebar: () => <div>ga-reports-settings</div>,
    /* main: () => (
      <Suspense fallback={<div>Loading page...</div>}>
        <Settings />
      </Suspense>
    ), */
  },
  login: {
    path: '/login',
    exact: true,
    isPublic: true,
    faIcon: 'fas fa-map-marked-alt',
    menuItemValue: 'ga-reports-settings',
    pageIdentifier: 'login',
    // sidebar: () => <div>ga-reports-settings</div>,
    /* main: () => () => (
      <Suspense fallback={<div>Loading page...</div>}>
        <Login />
      </Suspense>
    ), */
  },
};

export default ROUTES;
