import { ROUTES } from '../../module/configs/routes'; /* todo: load from main */

const sideBarMenu = [
  {
    title: ROUTES.dashboard.menuItemValue,
    path: ROUTES.dashboard.path,
  },
  {
    title: ROUTES.settings.menuItemValue,
    path: ROUTES.settings.path,
  },
];

export default sideBarMenu;
