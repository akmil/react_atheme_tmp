import React, { useState, useContext, createContext } from 'react';
import AuthContext from './MainContext/AuthContext';

const MainContext = createContext();

export function ProvideMainContext({ children }) {
  const provideMainContext = useProvideMainContext();
  return <MainContext.Provider value={provideMainContext}>{children}</MainContext.Provider>;
}

export const useMainContext = () => useContext(MainContext);

const adoptLayoutParams = (params = {}) => ({
  bodyClass: (params && params.bodyClass) ? params.bodyClass : '',
  pageTitle: (params && params.pageTitle) ? params.pageTitle : '',
  ...params,
});

function useProvideMainContext() {
  const authContext = AuthContext();
  const [user, setUser] = useState(() => authContext.getToken());
  /* const [lastActivity, setLastActivity] = useState(0); */
  const [layout, setLayout] = useState(adoptLayoutParams());

  const setAuthUser = (token, expiresIn, userRole = 'user') => {
    const fixedExpiresIn = (expiresIn && expiresIn?.toString().length > 10) ? expiresIn : expiresIn * 1000;
    setUser(authContext.prepareTokenJson(token, fixedExpiresIn, userRole));
    authContext.setToken(token, fixedExpiresIn, userRole);
  };

  const getStorageToken = () => authContext.getToken();

  const unsetAuthUser = () => {
    setAuthUser(null, null, null);
    /* setLastActivity(0); */
    authContext.clearToken();
  };

  const setLayoutParams = (params) => {
    setLayout(adoptLayoutParams(params));
  };

  /* const isActualToken = () => {
        authContext.isActualToken(user, unsetAuthUser);
    } */

  /* useEffect(() => {
        user && user.token && authContext.isActualToken(user, unsetAuthUser);
    }, [user]); */

  return {
    user,
    /* lastActivity, */
    layout,
    setAuthUser,
    unsetAuthUser,
    getStorageToken,
    /* setLastActivity, */
    setLayoutParams,
  };
}
