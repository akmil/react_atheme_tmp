// import React from 'react';
import HelperStorage from '../../utils/HelperStorage';

export default function AuthContext() {
  const storage = HelperStorage();
  // const storageLocal = HelperStorage(localStorage);

  const storageKey = {
    token: 'web-tools-token',
    expireIn: 'web-tools-expires-in',
    role: 'web-tools-role',
  };

  const prepareTokenJson = (token, expireIn, role = 'user') => ({ token, expires_in: expireIn, role });

  const getToken = () => {
    const token = (storage.getItem(storageKey.token)) ? storage.getItem(storageKey.token) : null;
    const expireIn = (storage.getItem(storageKey.expireIn)) ? storage.getItem(storageKey.expireIn) : null;
    const role = (storage.getItem(storageKey.role)) ? storage.getItem(storageKey.role) : null;

    return prepareTokenJson(token, expireIn, role);
  };

  const setToken = (token, expireIn, role) => {
    storage.setItem(storageKey.token, token);
    storage.setItem(storageKey.expireIn, expireIn);
    storage.setItem(storageKey.role, role);
  };

  const clearToken = () => {
    storage.removeItem(storageKey.token);
    storage.removeItem(storageKey.expireIn);
    storage.removeItem(storageKey.role);
  };

  /* useEffect(() => {
        mainContext.user && mainContext.user.token && isActualToken();
    }, [mainContext.user]); */

  /* const isActualToken = () => {

    } */

  /* useEffect(() => {

    }, []); */

  return {
    getToken,
    setToken,
    clearToken,
    prepareTokenJson,
  };
}
