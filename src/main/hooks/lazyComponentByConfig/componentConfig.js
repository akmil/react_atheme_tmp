const configAcronis = {
  id: 'acronis',
  name: 'acronis full name',
  components: {
    componentNameA: {
      data: {
        title: 'acronis - componentNameA',
      },
    },
    componentNameB: {
      data: {
        title: 'acronis - componentNameB',
      },
    },
    componentNameC: {
      data: {
        title: 'acronis - componentNameC',
      },
    },
  },
};

const configWebCom = {
  id: 'webCom',
  name: 'webCom full name',
  components: {
    componentNameA: {
      data: {
        title: 'webCom - componentNameA',
      },
    },
    componentNameC: {
      data: {
        title: 'webCom - componentNameC',
      },
    },
  },
};

export default { [configAcronis.id]: configAcronis, [configWebCom.id]: configWebCom };
