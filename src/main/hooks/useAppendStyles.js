import { useEffect } from 'react';

const useAppendStyles = ({ url, id, /* isGlobal = false, */ layoutSelector }) => {
  useEffect(() => {
    if (!document.getElementById(id)) {
      const scriptsAndStyles = document.querySelector(layoutSelector);
      scriptsAndStyles.innerHTML = `<link id="${id}" href="${url}" rel="stylesheet">`;
    }
  }, [url]);
};
export default useAppendStyles;
