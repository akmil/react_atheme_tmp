import React, { useState } from 'react';
// import componentConfig from './lazyComponentByConfig/componentConfig'; // demo-config

const LazyComponentList = ({ lazyComponents, componentConfig }) => Object.values(lazyComponents).map((LazyComponent) => (
  <LazyComponent key={componentConfig[LazyComponent.name].id} data={componentConfig[LazyComponent.name]} />
));

const useLazyConfigLoader = (importView) => {
  const [lazyComponents, setLazyComponent] = useState({});

  const addLazyComponents = (data) => {
    const { id } = data;
    if (lazyComponents[id]) {
      // skip loading if component was loaded
      return;
    }

    const LazyComponent = importView(id);
    LazyComponent.name = id;
    setLazyComponent(() => ({ ...lazyComponents, [id]: LazyComponent }));
  };

  return { lazyComponents, addLazyComponents, LazyComponentList };
};

export default useLazyConfigLoader;
