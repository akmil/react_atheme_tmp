import React, { useEffect } from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import { useMainContext } from '../contexts/MainContext';
import AuthService from '../services/AuthService/AuthService';

/* todo: move out of Main(pass as props) or Copy */
import Login from '../../module/Pages/login/Login';
import NotFound from '../../module/Pages/NotFound';
import routes from '../../module/configs/routes';
import routesToPages from '../configs/mapRouteToPage';

export default function Routes() {
  const mainContext = useMainContext();
  const authService = AuthService();

  /* console.log('routes', routes); */

  useEffect(() => {
    /* mainContext?.user?.token && authService.isActualToken(); */
    authService.isActualToken();
  }, []);

  return (
    <Router>
      <Switch>
        {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
          >
            {(route.isPublic || mainContext?.user?.token) ? routesToPages(route.pageIdentifier) : () => <Login />}
          </Route>
        ))}
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
}
