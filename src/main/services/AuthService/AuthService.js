import Network from '../Network/Network';
import HelperStorage from '../../utils/HelperStorage';
import { useMainContext } from '../../contexts/MainContext';
import appConfig from '../../../module/configs/config';

function AuthService() {
  const network = Network();
  const storage = HelperStorage();
  const mainContext = useMainContext();
  /* let lastActivity = new Date().getTime(); */
  let timerID;
  let intervalID;

  /* ############ - Helpers - ############# */
  const getTokenData = () => mainContext.getStorageToken();

  // const getContextUserToken = () => {
  //   return  mainContext.user.token;
  // };
  /* ############ - Helpers - ############# */

  const signIn = (email, password) => {
    const body = {
      login: email,
      password,
    };

    const requestOptions = network.prepareRequestOptions({ data: body });

    return network.sendRequest(appConfig.url.LOGIN_URL, requestOptions)
      .then((response) => {
        if (response.status === 'success') {
          const userRole = 'admin'; /* TODO */
          mainContext.setAuthUser(response.token, response.expires_in, userRole);
        }
        return response;
      }).catch((error) => {
        /* ToDo */
        // eslint-disable-next-line no-console
        console.log('error', error);
      });
  };

  const refreshToken = () => {
    const { token } = getTokenData();

    if (!token) {
      clearInterval(timerID);

      return false;
    }
    /* const requestOptions = network.prepareRequestOptions({}, mainContext.user.token); */
    const requestOptions = network.prepareRequestOptions({ token });
    network.sendRequest(appConfig.url.REFRESH_TOKEN, requestOptions)
      .then((response) => {
        if (response.status === 'success') {
          const userRole = 'admin'; /* TODO */
          mainContext.setAuthUser(response.token, response.expires_in, userRole);
        }
      }).catch((error) => {
      // eslint-disable-next-line no-console
        console.log('error', error);
      });

    return false;
  };

  const logOut = () => {
    const { token } = getTokenData();

    if (!token) {
      clearInterval(timerID);
      mainContext.unsetAuthUser();
      return false;
    }
    const requestOptions = network.prepareRequestOptions({ token });
    network.sendRequest(appConfig.url.DELETE_TOKEN, requestOptions)
      .then((response) => {
        if (response.status === 'success') {
          /* mainContext.unsetAuthUser(mainContext.lastActivity); */
          clearInterval(timerID);
          mainContext.unsetAuthUser();
        }
      }).catch((error) => {
      // eslint-disable-next-line no-console
        console.log('error', error);
        clearInterval(timerID);
        mainContext.unsetAuthUser();
      });
    return false;
  };

  const isActualToken = () => {
    // noinspection PointlessArithmeticExpressionJS
    const gapDelta = 1000 * 60 * 1; // 1 min; */
    const lastActiveDelta = 1000 * 60 * 9; // 9 mins; */
    const lastUserActiveKey = 'al-active';
    // const aActiveStateKey = "a-active";
    /* const aActiveState = (storage.getItem(aActiveStateKey)) ? storage.getItem(aActiveStateKey) : false; */

    const runEventListeners = () => {
      const eventHandler = () => {
        /* setLastActivity(new Date().getTime()); */
        /* lastActivity = new Date().getTime(); */
        const lastActivity = storage.getItem(lastUserActiveKey);
        const currentTime = new Date().getTime();

        if (currentTime - lastActivity > 500) {
          storage.setItem(lastUserActiveKey, new Date().getTime());
        }
      };

      window.addEventListener('mousemove', eventHandler);
      window.addEventListener('scroll', eventHandler);
      window.addEventListener('keydown', eventHandler);
    };

    runEventListeners();
    /* mainContext?.user?.token && runEventListeners(); */
    /* storage.setItem(aActiveStateKey, new Date().getTime()); */

    clearInterval(intervalID);

    intervalID = setInterval(() => {
      const tokenData = getTokenData();
      if (tokenData.token) {
        const lastActivity = storage.getItem(lastUserActiveKey);
        const currentTime = new Date().getTime();
        if ((tokenData.expires_in - currentTime < gapDelta) && (currentTime - lastActivity <= lastActiveDelta)) {
          refreshToken();
        } else if ((currentTime - lastActivity > lastActiveDelta) || (currentTime >= tokenData.expires_in)) {
          logOut();
        }
      }
    }, 60000);
  };

  return {
    signIn,
    logOut,
    refreshToken,
    isActualToken,
  };
}

export default AuthService;
