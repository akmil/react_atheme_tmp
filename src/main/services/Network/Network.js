function Network() {
  const prepareRequestOptions = ({
    data, method = 'POST', token, bearer,
  }) => {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    if (token) {
      myHeaders.append('Authorization', `Token ${token}`);
    } else if (bearer) {
      myHeaders.append('Authorization', `Bearer ${bearer}`);
    }

    const options = () => {
      if (method !== 'GET') {
        return {
          method,
          type: 'json',
          cache: 'no-cache',
          headers: myHeaders,
          body: JSON.stringify({
            ...data,
          }),
        };
      }

      // if GET
      return {
        method,
        type: 'json',
        cache: 'no-cache',
        headers: myHeaders,
      };
    };

    return options();
  };

  const sendRequest = (url, requestOptions) => fetch(url, requestOptions)
    .then((response) => {
      if (!response.ok) {
        /* const errMessage = `${response.status} ${response.statusText}`; */
        // if (cbError) {
        //     cbError(errMessage);
        // } else {
        //     this.cbErrorDefault(errMessage);
        // }
        /* console.error(errMessage); */

        throwError(response);
      }

      if (isStringResponse(response)) {
        return Promise.resolve(response.text());
      } if (isJSONResponse(response)) {
        return Promise.resolve(response.json());
      } if (isPdfResponse(response)) {
        return Promise.resolve(response.blob());
      } if (isImageResponse(response)) {
        return Promise.resolve(response.blob());
      }
      /* console.error('Content type is not supported'); */
      return false;
    })
    .catch((error) => Promise.reject(error.response));

  const throwError = (response) => {
    const error = new Error(response);
    error.response = response;
    error.status = response.status;
    if (response.status === 500) {
      /* console.error('Internal server Error'); */
    } else {
      /* console.error(response.status); */
    }
    throw error;
  };

  const isStringResponse = (response) => response.headers.get('content-type').indexOf('text/plain') !== -1;

  const isJSONResponse = (response) => response.headers.get('content-type').indexOf('application/json') !== -1;

  const isImageResponse = (response) => response.headers.get('content-type').indexOf('application/octet-stream') !== -1;

  const isPdfResponse = (response) => response.headers.get('content-type').indexOf('application/pdf') !== -1;

  return {
    sendRequest,
    prepareRequestOptions,
  };
}

export default Network;
