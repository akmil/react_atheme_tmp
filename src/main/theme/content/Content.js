import React from 'react';
/* import {useMainContext} from "../../contexts/MainContext"; */
import Title from './title/Title';

const Content = (props) => (
  <div className="main-content p-3">
    <Title />
    <div className="main-contant">
      {props.children}
    </div>
  </div>
);
export default Content;
