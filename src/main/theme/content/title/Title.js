import React, { useState, useEffect } from 'react';
import { useMainContext } from '../../../contexts/MainContext';

const Title = () => {
  const mainContext = useMainContext();
  const [pageTitle, setPageTitle] = useState();

  useEffect(() => {
    mainContext.layout.pageTitle && setPageTitle(mainContext.layout.pageTitle);
  }, [mainContext.layout]);

  return (

    <div className="page-title d-flex">
      <i className="bi-alarm" />
      <h3>{pageTitle}</h3>
    </div>
  );
};

export default Title;
