import React from 'react';
import AuthService from '../../services/AuthService/AuthService';

const Header = (/* props */) => {
  const authService = AuthService();

  return (
    <div className="navbar navbar-expand navbar-light navbar-bg navbar-static main-header p-3">
      <p>Main Header</p>
      <button type="button" className="btn btn-danger" onClick={() => authService.logOut()}>LogOut</button>
    </div>
  );
};

export default Header;
