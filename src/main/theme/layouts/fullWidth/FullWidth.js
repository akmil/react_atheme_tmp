import React from 'react';
// import 'bootstrap/dist/css/bootstrap.css';

const FullWidth = (props) => (
  <div className="container-fluid layout--full-width">
    {props.children}
  </div>
);

export default FullWidth;
