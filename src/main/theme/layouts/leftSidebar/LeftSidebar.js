import React from 'react';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap-icons/font/bootstrap-icons.css';
import Sidebar from '../../sidebar/Sidebar';
import Content from '../../content/Content';
import Header from '../../header/Header';
import useAppendStyles from '../../../hooks/useAppendStyles';

const LeftSidebar = (props) => {
  useAppendStyles({
    url: 'assets/vendor-styles/bootstrap-icons/bootstrap-icons.css',
    id: 'style-bootstrap',
    layoutSelector: '.layout--left-sidebar .styles-and-scripts',
    isGlobal: false,
  });

  /* useEffect(() => {

    }, [mainContext.layout]); */

  return (
    <div className="wrapper layout--left-sidebar">
      <Sidebar />
      <div className="content-wrapper">
        <Header />
        <Content>{props.children}</Content>
      </div>
      <div className="styles-and-scripts" />
    </div>
  );
};

export default LeftSidebar;
