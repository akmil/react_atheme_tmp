import React from 'react';
// import 'bootstrap/dist/css/bootstrap.css';

const OneColumn = (props) => (
  <div className="container d-flex flex-column layout--one-column">
    <div className="row">
      {props.children}
    </div>
    <div className="styles-and-scripts" />
  </div>
);

export default OneColumn;
