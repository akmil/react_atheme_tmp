import React from 'react';
import SidebarBrand from './sidebarBrand/SidebarBrand';
import SidebarContent from './sidebarContent/SidebarContent';

const Sidebar = (/* props */) => (
  <div className="sidebar p-3">
    <SidebarBrand />
    <SidebarContent />
  </div>
);

export default Sidebar;
