import React from 'react';

const SidebarBrand = () => (
  <div className="sidebar-brand navbar navbar-light bg-transparent navbar-static">
    <a href="/" className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
      <span className="fs-4">SidebarBrand</span>
    </a>
    <hr />
  </div>
);

export default SidebarBrand;
