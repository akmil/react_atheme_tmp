import React from 'react';
/* import {useMainContext} from "../../../contexts/MainContext/MainContext"; */
import SidebarMenu from './sidebarMenu/SidebarMenu';

const SidebarContent = () => (
  <div className="sidebar-content">
    <p>SideBar</p>
    <SidebarMenu />
  </div>
);

export default SidebarContent;
