import React from 'react';
import SidebarMenuItem from './SidebarMenuItem';
import sideBarMenu from '../../../../../module/configs/sideBarMenu';

const SidebarMenu = () => (
  <ul className="nav nav-pills flex-column mb-auto">
    {sideBarMenu.map((item, index) => <SidebarMenuItem item={item} key={index} />)}
  </ul>
);

export default SidebarMenu;
