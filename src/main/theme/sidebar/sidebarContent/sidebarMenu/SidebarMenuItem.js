import React from 'react';
import { Link } from 'react-router-dom';

const SidebarMenuItem = ({ item }) => (
  <li>
    <Link to={item.path} className="nav-link text-white">
      {item.title}
    </Link>
  </li>
);

export default SidebarMenuItem;
