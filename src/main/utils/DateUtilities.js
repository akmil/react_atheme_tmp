const DateUtilities = () => {
  const getCurrentYear = () => new Date().getFullYear();

  return {
    getCurrentYear,
  };
};
export default DateUtilities;
