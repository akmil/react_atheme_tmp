function HelperStorage(storageType) {
  const browserStorage = storageType || sessionStorage;

  const setItem = (key, value) => {
    const combineKey = makeStorageKey(key);
    browserStorage.setItem(combineKey, value);
  };

  const getItem = (key) => {
    const combineKey = makeStorageKey(key);
    return (browserStorage.getItem(combineKey)) ? browserStorage.getItem(combineKey) : null;
  };

  const removeItem = (key) => {
    const combineKey = makeStorageKey(key);
    browserStorage.removeItem(combineKey);
  };

  const clear = () => {
    browserStorage.clear();
  };

  const makeStorageKey = (key) => makeHash(getHost(), key);

  const getHost = () => document.location.host;

  const makeHash = (host, string = '') => `${window.btoa(host)}-${window.btoa(string)}`;

  return {
    setItem,
    getItem,
    removeItem,
    clear,
  };
}

export default HelperStorage;
