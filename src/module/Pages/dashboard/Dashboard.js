import React, { useEffect } from 'react';
import { useMainContext } from '../../../main/contexts/MainContext';
import LeftSidebar from '../../../main/theme/layouts/leftSidebar/LeftSidebar';
import useLazyConfigLoader from '../../../main/hooks/useLazyConfigLoader';
import importView from '../../utils/importView';
import isvConfigs from '../../utils/lazyComponentByConfig/componentConfig';

export default function Dashboard() {
  const mainContext = useMainContext();
  const { lazyComponents, addLazyComponents, LazyComponentList } = useLazyConfigLoader(importView);
  const loadAcronis = () => addLazyComponents(isvConfigs.lazyAppA); // isvConfigs.lazyAppA - expect to be predefined
  const loadWebCom = () => addLazyComponents(isvConfigs.lazyAppB);

  useEffect(() => {
    loadAcronis();
    loadWebCom();
  });

  useEffect(() => {
    mainContext.setLayoutParams({ bodyClass: 'page-dashboard', pageTitle: 'Dashboard' });
  }, []);

  return (
    <LeftSidebar>
      <div className="card bg-dark text-light">
        <div className="card-header"> Dashboard Widgets </div>
        <div className="card-body">
          Dashboard Page
        </div>
        <section className="container">
          <React.Suspense fallback="Loading charts...">
            <div className="row">
              <LazyComponentList lazyComponents={lazyComponents} componentConfig={isvConfigs} />
            </div>
          </React.Suspense>
        </section>
      </div>
    </LeftSidebar>
  );
}
