import React from 'react';
import HeroText from './heroText/HeroText';
import LoginForm from './loginForm/LoginForm.tsx';
import OneColumn from '../../../main/theme/layouts/oneColumn/OneColumn';

export default function Login() {
  return (
    <OneColumn>
      <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
        <div className="align-middle d-table-cell">
          <HeroText />

          <div className="card">
            <div className="card-body">
              <div className="m-sm-4">
                <LoginForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    </OneColumn>
  );
}
