import React from 'react';

export default function HeroText() {
  return (
    <div className="text-center mt-4">
      <h1 className="h2">Welcome back</h1>
      <p className="lead">
        Sign in to your account to continue
      </p>
    </div>
  );
}
