import React, { useState } from 'react';

// import { useHistory } from "react-router-dom";

import AuthService from '../../../../main/services/AuthService/AuthService';
import { useMainContext } from '../../../../main/contexts/MainContext';

export default function LoginForm() {
  const [state, setState] = useState({
    email: '',
    password: '',
  });

  // const mainContext = useMainContext();
  // const history = useHistory();
  const auth = AuthService();

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = target;
    setState({
      ...state,
      [target.name]: value,
    });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const form = event.currentTarget;

    event.preventDefault();
    event.stopPropagation();

    if (form.checkValidity()) {
      auth.signIn(state.email, state.password).then((response) => {
        if (response.status === 'success') {

          /* mainContext?.user?.token && console.log("*****************************");

                    mainContext?.user?.token && authService.isActualToken(); */

          /*
                    const location = {
                        pathname: '/'
                    };

                    mainContext.user && mainContext.user.token && authService.isActualToken() && history.push(location); */

        } else {
          /* ToDo */
        }
      }).catch((error) => {
        console.log('error', error);
      });
    } else {
      form.classList.add('was-validated');
    }
  };

  return (
    <form className="form-signin" noValidate onSubmit={handleSubmit}>
      <div className="mb-3">
        <label className="form-label">Email</label>
        <input
          className="form-control form-control-lg"
          type="email"
          name="email"
          placeholder="Enter your email"
          onChange={handleChange}
          required
        />
        <div className="invalid-feedback">
          Please input your Email.
        </div>
      </div>
      <div className="mb-3">
        <label className="form-label">Password</label>
        <input
          className="form-control form-control-lg"
          type="password"
          name="password"
          placeholder="Enter your password"
          onChange={handleChange}
          required
        />
        <div className="invalid-feedback">
          Please input your Password.
        </div>
        {/* <small>
                    <a href="pages-reset-password.html">Forgot password?</a>
                </small> */}
      </div>
      <div className="text-center mt-3">
        <button type="submit" className="btn btn-lg btn-primary">Sign in</button>
      </div>
    </form>
  );
}
