import React, { useEffect } from 'react';
import { useMainContext } from '../../../main/contexts/MainContext';
import LeftSidebar from '../../../main/theme/layouts/leftSidebar/LeftSidebar';

export default function Settings() {
  const mainContext = useMainContext();

  useEffect(() => {
    mainContext.setLayoutParams({ bodyClass: 'page-settings', pageTitle: 'Settings' });
  }, []);

  return (
    <LeftSidebar>
      <div className="card bg-dark text-light">
        <div className="card-header"> Card Header </div>
        <div className="card-body">
          Settings Page
        </div>
      </div>
    </LeftSidebar>
  );
}
