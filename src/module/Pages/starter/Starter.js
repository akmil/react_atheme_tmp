import React, { useEffect } from 'react';
import { useMainContext } from '../../../main/contexts/MainContext';
import LeftSidebar from '../../../main/theme/layouts/leftSidebar/LeftSidebar';

const Starter = () => {
  const mainContext = useMainContext();

  useEffect(() => {
    mainContext.setLayoutParams({ bodyClass: 'page-starter', pageTitle: 'Starter' });
  }, []);

  return (
    <LeftSidebar>
      <div className="card bg-dark text-light">
        <div className="card-header"> Card Header </div>
        <div className="card-body">
          Starter Page
        </div>
      </div>
    </LeftSidebar>
  );
};

export default Starter;
