import defaultConfig from '../../main/configs/config';
import DateUtilities from '../../main/utils/DateUtilities';

const dateUtils = DateUtilities();
const url = {
  ...defaultConfig.url,
  LOGIN_URL: 'http://localhost:4008/api/v1/auth/login/',
  DELETE_TOKEN: 'http://localhost:4008/api/v1/auth/del_token/',
  REFRESH_TOKEN: 'http://localhost:4008/api/v1/auth/access_token/',
};

const app = Object.freeze({
  ...defaultConfig.app,
  copyright: `Copyright © 2021 - ${dateUtils.getCurrentYear()}`,
});

const config = {
  app,
  url,
};

export default config;
