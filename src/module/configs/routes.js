import defaultRoutes from '../../main/configs/routes';

const ROUTES = {
  ...defaultRoutes,
  dashboard: { ...defaultRoutes.dashboard, isPublic: true },
};

const ROUTES_AS_ARRAY = Object.keys(ROUTES).map((routeKey) => ROUTES[routeKey]);

export default ROUTES_AS_ARRAY;
export { ROUTES };
