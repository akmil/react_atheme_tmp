import defaultSideBarMenu from '../../main/configs/sideBarMenu';
// import {ROUTES} from './routes';

const sideBarMenu = [
  ...defaultSideBarMenu,
];

export default sideBarMenu;
