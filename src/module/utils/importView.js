import { lazy } from 'react';

const importView = (name) => lazy(() => import(`./../../module/components/${name}/lazyApp`)
  .catch(() => import('./lazyComponentByConfig/NotFound')));

export default importView;
