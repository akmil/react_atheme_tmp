import React from 'react';

const NotFound = () => <div>Page is NotFound (check you are logged)</div>;

export default NotFound;
