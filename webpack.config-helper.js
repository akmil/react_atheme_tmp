// import {IgnorePlugin} from "webpack";

const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


// config htmlWebpackPlugin example: https://github.com/jaketrent/html-webpack-template/blob/86f285d5c7/index.html
const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: path.join(__dirname, './src/index.html'),
    filename: './index.html',
    title: 'companyName',
    // window: {
    //     env: {
    //         [parentSystem]: parentSystem,
    //         script: bxScript
    //     }
    // }
});

// const htmlWebpackPluginInstallPage = new HtmlWebpackPlugin({
//     template: path.join(__dirname, './install-page/install.html'),
//     filename: './install.html',
//     excludeChunks: ['index']
// });


module.exports = options => {
    const {ignoreAssetsBuild, devtool, isProduction} = options;
    console.log('options ---->', options);

    const webpackConfig = {
        devtool: devtool || false,
        entry: {
            index: [
                path.join(__dirname, './src/index.js')
            ],
            // install: [
            //     path.join(__dirname, './install-page/install.js')
            // ],
        },
        output: {
            path: path.join(__dirname, './_dist'),
            filename: './[name].js',
            chunkFilename: `./assets/js/components/[name].[contenthash].chunk.js`,
        },
        optimization: {
            chunkIds: 'named',
            mergeDuplicateChunks: true,
            moduleIds: 'deterministic', // new webpack.HashedModuleIdsPlugin(),
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    // Customer vendor
                    defaultVendors: {
                        test: /[\\/]node_modules[\\/](.(?!.*\.css$))*$/,
                        name: 'vendors-bundle',
                        chunks: 'initial',
                        enforce: true
                    },
                }
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: [
                            '@babel/transform-runtime',
                            '@babel/plugin-proposal-class-properties'
                        ]
                    }
                },
                {
                    test: /\.(ts|tsx)$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript'],
                        plugins: [
                            '@babel/transform-runtime',
                            '@babel/plugin-proposal-class-properties',

                        ]
                    }
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'fonts/'
                            }
                        }]
                },
                {
                    test: /\.css$/,
                    use: ["style-loader", "css-loader"]
                }
            ]
        },
        plugins: [
            htmlWebpackPlugin,
            // new webpack.HashedModuleIdsPlugin() used in v.4
        ],
        resolve: {
            extensions: ['.js', '.jsx', '.scss']
        },
        devServer: {
            port: 3008
        }
    };

    if (!ignoreAssetsBuild) {
        webpackConfig.entry.index.push(path.join(__dirname, './scss/index.scss'));
        // webpackConfig.entry.install.push(path.join(__dirname, './scss/index.scss'));

        webpackConfig.module.rules.push({
            test: /\.scss$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        outputPath: './assets/css/',
                        name: '[name].min.css'
                    }
                },
                'sass-loader'
            ]
        });
        // webpackConfig.plugins.push(new CleanWebpackPlugin());
        webpackConfig.plugins.push(new CopyWebpackPlugin({ patterns: [
                { from: './assets', to: `./assets` },
            ]}));
    }
    if(isProduction){
        webpackConfig.optimization.minimizer = [
            new TerserPlugin({
                //sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    compress: {
                        drop_console: true,
                    },
                },
            }),
        ];
    }
    return webpackConfig;
};
