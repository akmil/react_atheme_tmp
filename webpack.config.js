const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const WebSiteName = "WebTools";

const htmlWebpackPlugin = new HtmlWebpackPlugin({
    /*template: path.join(__dirname, './index.html'),*/
    template: path.resolve(__dirname, "src", "index.html"),
    /*filename: './index.html',*/
    title: WebSiteName,
    /*window: {
        env: {
            [parentSystem]: parentSystem,
            script: bxScript
        }
    }*/
});

module.exports = {
    devtool: 'eval-source-map', // dev only
    entry: {
        index: [
            path.resolve(__dirname, './src/index.js'),
            path.resolve(__dirname, './scss/index.scss'),
        ],
    },
    output: {
        path: path.resolve(__dirname, './_dist'),
        filename: '.assets/js/[name].js',
        chunkFilename: `./assets/js/components/[name].chunk.js`,
        publicPath: '/'
    },
    optimization: {
        chunkIds: 'named',
        mergeDuplicateChunks: false,
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/](.(?!.*\.css$))*$/,
                    name: 'vendors-bundle',
                    chunks: 'initial',
                    enforce: true
                },
            }
        }/*,
        minimizer: [
            new TerserPlugin({
                sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    compress: {
                    },
                },
            }),
        ]*/

    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react'],
                    plugins: [
                        '@babel/transform-runtime',
                        '@babel/plugin-proposal-class-properties',

                    ]
                }
            },
            {
                test: /\.(ts|tsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript'],
                    plugins: [
                        '@babel/transform-runtime',
                        '@babel/plugin-proposal-class-properties',

                    ]
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    'style-loader', // creates style nodes from JS strings
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                        options: {
                            importLoaders: 1
                        }
                        /*loader: 'file-loader',
                        options: { outputPath: 'assets/css/', name: '[name].min.css'}*/
                    },
                    'postcss-loader', // post process the compiled CSS
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    plugins: [
        htmlWebpackPlugin,
        new CleanWebpackPlugin(),
        /*new CopyWebpackPlugin({ patterns:[
            {from: './assets', to: './assets'},
        ]}),*/

    ],
    resolve: {
        extensions: ['.js', '.jsx', '.scss']
    },

    devServer: {
        historyApiFallback: true, // need for {BrowserRouter} from "react-router-dom"
        // proxy: {
        //     "/":  {
        //         "target": "https://localhost:4800",
        //         "secure": "false"
        //     }
        // }

        host: "localhost",
        port: 4008,
        https: false
    },
    externals: {
        'ConfigDataAppPath': './content/App.js'
    }
};
